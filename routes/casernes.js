var express = require('express');
var router = express.Router();
const db = require('./../models')
const Casernes = db.models.casernes

const isAuthenticated = (req, res, next) => {
  // if user is authenticated in the session, call the next() to call the next request handler 
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect('/login');
}

router.get('/', (req, res, next) => {
  Casernes.findAll().then((casernes) => res.json(casernes)).catch((err) => {
    return next(err)
  })
})

router.get('/:id([0-9]+)', (req, res, next) => {
  if (!req.params.id) return next('NOT FOUND')
  Casernes.findByPk(req.params.id).then((caserne) => res.json(caserne)).catch((err) => {
    return next(err)
  })
})

router.post('/', (req, res, next) => {
  if (!req.body || (req.body && (!req.body.name && !req.body.frequency)))
    return next("NOT FOUND")

  Casernes.create(req.body).then((caserne) => res.json(caserne)).catch((err) => {
    return next(err)
  })
})

router.put('/:id([0-9]+)', (req, res, next) => {
  if (!req.params.id) return res.status(404).send('NOT FOUND')
  req.body.updated_at = new Date() // Update time
  req.body.id = req.params.id // Add id to body
  
  Casernes.update(req.body, { where: { id: req.params.id } }, { returning: true })
    .then(() => {
      return Casernes.findById(req.params.id)
    }).then((caserne) => res.json(caserne)).catch((err) => {
    return next(err)
  })
})

router.delete('/:id([0-9]+)', (req, res, next) => {
  if (!req.params.id) return res.status(404).send('NOT FOUND')
  Casernes.destroy({ where: { id: req.params.id } }).then(() => res.json({ message: 'User supprimée avec succès' })).catch((err) => {
    return next(err)
  })
})

router.get('/:id([0-9]+)/edit', isAuthenticated, async (req, res, next) => {
  let caserne = ''
  try {
    caserne = await Casernes.findByPk(req.params.id)
  } catch (error) {
    console.log(error)
  }
  res.render('casernes/edit', { title: 'Édition caserne', loggedUser: req.user, caserne });
});

router.get('/add', isAuthenticated, async (req, res, next) => {
  res.render('casernes/add', { title: 'Ajouter une caserne', loggedUser: req.user });
});

router.get('/auth', (req, res, next) => {
  if (!req.query.token) return next('NOT FOUND')
  Casernes.findOne({ where: { token: req.query.token }}).then((caserne) => res.json(caserne)).catch((err) => {
    return next(err)
  })
})

module.exports = router