var express = require('express');
var router = express.Router();
const db = require('./../models')
const Users = db.models.users
const Casernes = db.models.casernes

const isAuthenticated = (req, res, next) => {
  // if user is authenticated in the session, call the next() to call the next request handler 
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect('/login');
}

router.get('/', (req, res, next) => {
  Users.findAll().then((users) => res.json(users)).catch((err) => {
    return next(err)
  })
})

router.get('/:id([0-9]+)', (req, res, next) => {
  if (!req.params.id) return next('NOT FOUND')
  Users.findByPk(req.params.id).then((user) => res.json(user)).catch((err) => {
    return next(err)
  })
})

router.post('/', (req, res, next) => {
  if (!req.body || (req.body && (!req.body.username && !req.body.password)))
    return next("NOT FOUND")

  Users.create(req.body).then((user) => res.json(user)).catch((err) => {
    return next(err)
  })
})

router.put('/:id([0-9]+)', (req, res, next) => {
  if (!req.params.id) return res.status(404).send('NOT FOUND')
  req.body.updated_at = new Date() // Update time
  req.body.id = req.params.id // Add id to body
  
  Users.update(req.body, { where: { id: req.params.id } }, { returning: true })
    .then(() => {
      return Users.findById(req.params.id)
    }).then((user) => res.json(user)).catch((err) => {
    return next(err)
  })
})

router.delete('/:id([0-9]+)', (req, res, next) => {
  if (!req.params.id) return res.status(404).send('NOT FOUND')
  Users.destroy({ where: { id: req.params.id } }).then(() => res.json({ message: 'User supprimée avec succès' })).catch((err) => {
    return next(err)
  })
})

router.get('/:id([0-9]+)/edit', isAuthenticated, async (req, res, next) => {
  let user, casernes = ''
  try {
    user = await Users.findOne({
      include: [{
        model: Casernes,
      }],
      where: { id: req.params.id },
    })
    casernes = await Casernes.findAll()
  } catch (error) {
    console.log(error)
  }
  res.render('users/edit', { title: 'Édition utilisateur', loggedUser: req.user, user, casernes });
});

router.get('/add', isAuthenticated, async (req, res, next) => {
  try {
    casernes = await Casernes.findAll()
  } catch (error) {
    console.log(error)
  }
  res.render('users/add', { title: 'Ajouter un utilisateur', loggedUser: req.user, casernes });
});

module.exports = router