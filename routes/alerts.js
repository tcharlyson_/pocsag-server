var express = require('express');
var router = express.Router();
const db = require('./../models')
const Alerts = db.models.alerts

router.get('/', (req, res, next) => {
  Alerts.findAll().then((alerts) => res.json(alerts)).catch((err) => {
    return next(err)
  })
})

router.get('/last', (req, res, next) => {
  if (!req.query.message && !req.query.configurationId) return next('NOT FOUND')
  Alerts.findAll(
    { 
      where: { message: req.query.message, configurationId: req.query.configurationId },
      limit: 1,
      order: [
        ['createdAt', 'DESC']
      ]
    }).then((alert) => res.json(alert)).catch((err) => {
    return next(err)
  })
})

router.get('/:id', (req, res, next) => {
  if (!req.params.id) return next('NOT FOUND')
  Alerts.findByPk(req.params.id).then((alert) => res.json(alert)).catch((err) => {
    return next(err)
  })
})

router.post('/', (req, res, next) => {
  if (!req.body || (req.body && (!req.body.id_configuration && !req.body.message)))
    return next("NOT FOUND")

  Alerts.create(req.body).then((alert) => {
    let roomId = (req.query.token) ? req.query.token : 'admin'
    // SOCKETS
    req.app.get('socketio').to('/' + roomId).emit('new-alert', alert);
    req.app.get('socketio').to('/admin').emit('new-alert', alert);
    res.json(alert)
  }).catch((err) => {
    return next(err)
  })
})

router.put('/:id', (req, res, next) => {
  if (!req.params.id) return res.status(404).send('NOT FOUND')
  req.body.id = req.params.id // Add id to body
  
  Alerts.update(req.body, { where: { id: req.params.id } }, { returning: true })
    .then(() => {
      return Alerts.findById(req.params.id)
    }).then((alert) => {
    // SOCKETS
    let roomId = (req.query.token) ? req.query.token : 'admin'
    req.app.get('socketio').to('/' + roomId).emit('acknowledge-alert', alert);
    req.app.get('socketio').to('/admin').emit('acknowledge-alert', alert);

    res.json(alert)
  }).catch((err) => {
    return next(err)
  })
})

router.delete('/:id', (req, res, next) => {
  if (!req.params.id) return res.status(404).send('NOT FOUND')
  Alerts.destroy({ where: { id: req.params.id } }).then(() => res.json({ message: 'Alerte supprimée avec succès' })).catch((err) => {
    return next(err)
  })
})

module.exports = router
