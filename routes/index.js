var express = require('express');
var router = express.Router();
const db = require('./../models')
const Alerts = db.models.alerts
const Configurations = db.models.configurations
const passport = require('passport')

const isAuthenticated = (req, res, next) => {
  // if user is authenticated in the session, call the next() to call the next request handler 
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect('/login');
}

/* GET home page. */
router.get('/', isAuthenticated, async (req, res, next) => {
  let alerts = ''
  try {
    if (req.user.role == 'SUPER')
      alerts = await Alerts.findAll({
        include: [{
          model: Configurations,
        }],
        order: [
          ['id', 'DESC'],
        ],
      })
    else
      alerts = await Alerts.findAll({
        include: [{
          model: Configurations,
          where: { caserneId: req.user.caserneId }
        }],
        order: [
          ['id', 'DESC'],
        ],
        limit: 50
      })
  } catch (error) {
    console.log(error)
  }
  res.render('index', { loggedUser: req.user, title: 'Liste des alertes', alerts });
});

router.get('/login', (req, res, next) => {
  res.render('login', { title: 'Connexion', message: req.flash('error') });
});

router.post('/login',
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
  })
);

router.get('/logout', isAuthenticated, (req, res) => {
  req.logout();
  res.redirect('/');
});

module.exports = router;
