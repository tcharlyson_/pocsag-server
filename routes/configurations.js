var express = require('express');
var router = express.Router();
const db = require('./../models')
const Configurations = db.models.configurations
const Casernes = db.models.casernes


const isAuthenticated = (req, res, next) => {
  // if user is authenticated in the session, call the next() to call the next request handler 
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect('/login');
}

router.get('/', (req, res, next) => {
  Configurations.findAll().then((configurations) => res.json(configurations)).catch((err) => {
    return next(err)
  })
})

router.get('/:id([0-9]+)', (req, res, next) => {
  if (!req.params.id) return next('NOT FOUND')
  Configurations.findByPk(req.params.id).then((configuration) => res.json(configuration)).catch((err) => {
    return next(err)
  })
})

router.get('/credentials', (req, res, next) => {
  if (!req.query.capcode) return next('NOT FOUND')
  Configurations.findOne({ where: { capcode: req.query.capcode }}).then((configuration) => res.json(configuration)).catch((err) => {
    return next(err)
  })
})


router.post('/', (req, res, next) => {
  if (!req.body || (req.body && (!req.body.capcode)))
    return next("NOT FOUND")

  Configurations.create(req.body).then((employee) => res.json(employee)).catch((err) => {
    return next(err)
  })
})

router.put('/:id([0-9]+)', (req, res, next) => {
  if (!req.params.id) return res.status(404).send('NOT FOUND')
  req.body.updated_at = new Date() // Update time
  req.body.id = req.params.id // Add id to body
  
  Configurations.update(req.body, { where: { id: req.params.id } }, { returning: true })
    .then(() => {
      return Configurations.findById(req.params.id)
    }).then((configuration) => res.json(configuration)).catch((err) => {
    return next(err)
  })
})

router.delete('/:id', (req, res, next) => {
  if (!req.params.id) return res.status(404).send('NOT FOUND')
  Configurations.destroy({ where: { id: req.params.id } }).then(() => res.json({ message: 'Configuration supprimée avec succès' })).catch((err) => {
    return next(err)
  })
})

router.get('/add', isAuthenticated, async (req, res, next) => {
  try {
    if (req.user.role == 'SUPER')
      casernes = await Casernes.findAll()
    else
      casernes = await Casernes.findAll({ where: { id: req.user.caserneId }})
  } catch (error) {
    console.log(error)
  }
  res.render('configurations/add', { title: 'Ajouter une configuration', loggedUser: req.user, casernes });
});


router.get('/:id([0-9]+)/edit', isAuthenticated, async (req, res, next) => {
  let configuration, casernes = ''
  try {
    configuration = await Configurations.findOne({
      include: [{
        model: Casernes,
      }],
      where: { id: req.params.id },
    })
    if (req.user.role == 'SUPER')
      casernes = await Casernes.findAll()
    else
      casernes = await Casernes.findAll({ where: { id: req.user.caserneId } })
  } catch (error) {
    console.log(error)
  }
  res.render('configurations/edit', { title: 'Édition configuration', loggedUser: req.user, configuration, casernes });
});

module.exports = router