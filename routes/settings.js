var express = require('express');
var router = express.Router();
const db = require('../models')
const Users = db.models.users
const Configurations = db.models.configurations
const Casernes = db.models.casernes

const isAuthenticated = (req, res, next) => {
  // if user is authenticated in the session, call the next() to call the next request handler 
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    if (req.user.role == 'SUPER' || req.user.role == 'ADMIN')
      return next();
    else
      return next({ message: 'Une erreur est survenue', status: 404 });
  // if the user is not authenticated then redirect him to the login page
  res.redirect('/login');
}

const isAuthenticatedAndSuper = (req, res, next) => {
  // if user is authenticated in the session, call the next() to call the next request handler 
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    if (req.user.role == 'SUPER')
      return next();
    else
      return next({ message: 'Une erreur est survenue', status: 404 });
  // if the user is not authenticated then redirect him to the login page
  res.redirect('/login');
}

router.get('/', isAuthenticatedAndSuper, async (req, res, next) => {
  let users = ''
  try {
    users = await Users.findAll({
      include: [{
        model: Casernes,
      }],
      where: { id: { $ne: req.user.id }}
    })
  } catch (error) {
    console.log(error)
  }
  res.render('settings/users', { title: 'Paramètres', loggedUser: req.user, users });
});

router.get('/casernes', isAuthenticatedAndSuper, async (req, res, next) => {
  let casernes = ''
  try {
    casernes = await Casernes.findAll({ where: { name: { $ne: 'administration' } }})
  } catch (error) {
    console.log(error)
  }
  res.render('settings/casernes', { title: 'Paramètres', loggedUser: req.user, casernes });
});

router.get('/configurations', isAuthenticated, async (req, res, next) => {
  let configurations = ''
  try {
    if (req.user.role == 'SUPER')
      configurations = await Configurations.findAll({
        include: [{
          model: Casernes,
        }],
      })
    else
      configurations = await Configurations.findAll({
        include: [{
          model: Casernes,
        }],
        where: { caserneId: req.user.caserneId }
      })
  } catch (error) {
    console.log(error)
  }
  res.render('settings/configurations', { title: 'Paramètres', loggedUser: req.user, configurations });
});

module.exports = router
