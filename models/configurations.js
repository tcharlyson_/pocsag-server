module.exports = (sequelize, DataTypes) => {
  let Configuration = sequelize.define('configurations', {
    capcode: DataTypes.INTEGER,
    alias: DataTypes.STRING,
    pushover_key: DataTypes.STRING,
    pushover_token: DataTypes.STRING,
  })
  
  Configuration.associate = function (models) {
    models.configurations.hasMany(models.alerts)
    models.configurations.belongsTo(models.casernes, { onDelete: 'cascade' })
  }

  return Configuration;
}