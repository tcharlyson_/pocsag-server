module.exports = (sequelize, DataTypes) => {
  let Alert = sequelize.define('alerts', {
    message: DataTypes.STRING,
    notification_sent: DataTypes.DATE,
    acknowledged: DataTypes.BOOLEAN,
    acknowledged_at: DataTypes.DATE,
  })

  Alert.associate = function (models) {
    models.alerts.belongsTo(models.configurations, { onDelete: 'cascade' })
  }

  return Alert;
}