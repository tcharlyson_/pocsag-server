module.exports = (sequelize, DataTypes) => {
  let Caserne = sequelize.define('casernes', {
    name: DataTypes.STRING,
    frequency: DataTypes.STRING,
    token: DataTypes.STRING
  })
  
  Caserne.associate = function (models) {
    models.casernes.hasMany(models.configurations)
    models.casernes.hasMany(models.users)
  }

  Caserne.beforeCreate((caserne, options) => {
    const buf = require('crypto').randomBytes(48);
    caserne.token = buf.toString('hex')
    return sequelize.Promise.resolve(caserne);
  });

  return Caserne;
}