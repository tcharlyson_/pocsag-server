const bcrypt = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  let User = sequelize.define('users', {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING,
  })

  User.associate = function (models) {
    models.users.belongsTo(models.casernes, { onDelete: 'cascade' })
  }

  User.beforeCreate(async (user, options) => {
    const hash = await bcrypt.hash(user.password, 10)
    user.password = hash;
    return sequelize.Promise.resolve(user);
  });

  return User;
}