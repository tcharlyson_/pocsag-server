const Sequelize = require('sequelize')

const sequelize = new Sequelize(process.env.DATABASE_URL || 'postgres://pocsag_admin:Sup3@dmin@localhost:5432/pocsag')
const Alert = sequelize.import(__dirname + '/alerts.js')
const Configuration = sequelize.import(__dirname + '/configurations.js')
const Caserne = sequelize.import(__dirname + '/casernes.js')
const User = sequelize.import(__dirname + '/users.js')

module.exports = {
  sequelize,
  models: {
    alerts: Alert,
    configurations: Configuration,
    users: User,
    casernes: Caserne
  }
}