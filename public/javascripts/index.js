$(document).ready(() => {
  this.showModal = (type, method, id) => {
    selector = $('.modal__' + method);
    selector.addClass('is-active');

    if (method == 'edit') {
      url = '/' + type + '/' + id + '/' + method
      selector.find('.modal-card-body').load(url);
    }

    if (method == 'add') {
      url = '/' + type + '/' + method
      selector.find('.modal-card-body').load(url);
    }

    $('.modal__close').on('click', () => { 
      selector.removeClass('is-active');
    });
    $('.modal__cancel').on('click', () => {
      selector.removeClass('is-active');
    });
    selector.find('.modal__ok').on('click', () => {
      switch (method) {
        case 'delete':
          this.delete('/' + type + '/' + id, selector)
          break;
        case 'add':
          this.add('/' + type, selector)
          break;
        case 'edit':
          this.edit('/' + type + '/' + id, selector)
          break;
        default:
          break;
      }
    });
  }

  this.delete = (url, selector) => {
    $.ajax({
      url: url,
      method: 'DELETE'
    }).done(() => {
      selector.removeClass('is-active');
      location.reload();
    })
  }

  this.edit = (url, selector) => {
    data = selector.find('.modal__form').serialize()
    $.ajax({
      url: url,
      method: 'PUT',
      data: data
    }).done(() => {
      selector.removeClass('is-active');
      location.reload();
    })
  }

  this.add = (url, selector) => {
    data = selector.find('.modal__form').serialize()
    $.ajax({
      url: url,
      method: 'POST',
      data: data
    }).done(() => {
      selector.removeClass('is-active');
      location.reload();
    })
  }

  $(".navbar-burger").click(() => {
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
  })
})