var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const db = require('./models')
const bcrypt = require('bcrypt')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;
const flash = require("connect-flash");
const favicon = require("serve-favicon")

/* DATABASE INITIALIZATION */
Object.keys(db.models).forEach(modelName => {
  db.models[modelName].associate(db.models)
})

db.sequelize.sync()
  .then(() => console.log('Database synchronised'))
  .catch((e) => console.log(e))
/* __________ */

var indexRouter = require('./routes/index');
var alertsRouter = require('./routes/alerts');
var configurationsRouter = require('./routes/configurations');
var casernesRouter = require('./routes/casernes');
var settingsRouter = require('./routes/settings');
var usersRouter = require('./routes/users');


var app = express();

app.locals.moment = require('moment-timezone');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

/* SESSION */
app.use(require('express-session')({ secret: 'pocsag', resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
/* _______ */

/* PASSPORT */
passport.use(new LocalStrategy(async (username, password, done) => {
  let user = await db.models.users.findOne({
    where: { username },
    include: [{
      model: db.models.casernes,
      required: false
    }],
  })
  if (!user) return done(null, false, { message: 'Mauvais identifiant' });
  const isGoodPassword = await bcrypt.compare(password, user.password);
  if (!isGoodPassword) return done(null, false, { message: 'Mot de pass incorrect' });
  return done(null, user);
}));

passport.serializeUser((user, done) => {
  return done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  let user = await db.models.users.findOne({
    where: { id },
    include: [{
      model: db.models.casernes,
      required: false
    }],
  })
  if (!user) return done(null, false);
  return done(null, user);
});
/* _______ */

app.use(express.static(path.join(__dirname, 'public')));
app.use('/bulma', express.static(__dirname + '/node_modules/bulma'));
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));

app.all('/', (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

app.use('/', indexRouter);
app.use('/alerts', alertsRouter);
app.use('/configurations', configurationsRouter);
app.use('/settings', settingsRouter);
app.use('/users', usersRouter);
app.use('/casernes', casernesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
