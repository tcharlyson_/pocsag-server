# Pocsag interface admin

### Database

Créer une base postgres
Donner les privilièges sur la base au bon utilisateur
Créer un administrateur avec un role 'SUPER'
- docker exec -it $id_container sh
- bash
- psql -U pocsag_admin pocsag
INSERT INTO users(username, password, role, "createdAt", "updatedAt", "caserneId") VALUES('tcharlyson', '$2y$10$RBt1sgKIMHWPQJA2m1KUTuaGr03hrKAujHRCodCFRDsIEeYRNwChm', 'SUPER', current_timestamp, current_timestamp, null);